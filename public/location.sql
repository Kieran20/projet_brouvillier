-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1build0.15.04.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 23, 2018 at 05:46 PM
-- Server version: 5.6.28-0ubuntu0.15.04.1
-- PHP Version: 5.6.4-4ubuntu6.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `location`
--

-- --------------------------------------------------------

--
-- Table structure for table `Activite`
--

CREATE TABLE IF NOT EXISTS `Activite` (
`idActivite` int(3) NOT NULL,
  `nomActivite` varchar(255) NOT NULL,
  `descriptionActivite` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MomentReservation`
--

CREATE TABLE IF NOT EXISTS `MomentReservation` (
`idMomentReservation` int(20) NOT NULL,
  `idReservation` int(100) NOT NULL,
  `nbreJour` double NOT NULL,
  `idMomentSemaine` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `momentSemaine`
--

CREATE TABLE IF NOT EXISTS `momentSemaine` (
`idMomentSemaine` int(1) NOT NULL,
  `nomMomentSemaine` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `momentSemaine`
--

INSERT INTO `momentSemaine` (`idMomentSemaine`, `nomMomentSemaine`) VALUES
(1, 'semaine'),
(2, 'week_end');

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE IF NOT EXISTS `reservation` (
`idReservation` int(100) NOT NULL,
  `idUser` int(100) NOT NULL,
  `debutReservation` datetime NOT NULL,
  `finReservation` datetime NOT NULL,
  `nbrePersonne` int(2) NOT NULL,
  `prixReservation` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservation`
--

INSERT INTO `reservation` (`idReservation`, `idUser`, `debutReservation`, `finReservation`, `nbrePersonne`, `prixReservation`) VALUES
(1, 2, '2018-01-16 00:00:00', '2018-01-17 00:00:00', 5, 50),
(2, 2, '2018-01-04 00:00:00', '2018-01-13 00:00:00', 6, 150);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
`idRole` int(1) NOT NULL,
  `nomRole` varchar(50) NOT NULL,
  `rangRole` int(2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`idRole`, `nomRole`, `rangRole`) VALUES
(1, 'Administrateur', 100),
(2, 'Utilisateur', 25),
(3, 'visiteur', 1);

-- --------------------------------------------------------

--
-- Table structure for table `saison`
--

CREATE TABLE IF NOT EXISTS `saison` (
`idSaison` int(1) NOT NULL,
  `nomSaison` varchar(9) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `saison`
--

INSERT INTO `saison` (`idSaison`, `nomSaison`) VALUES
(1, 'été'),
(2, 'automne'),
(3, 'hiver'),
(4, 'printemps');

-- --------------------------------------------------------

--
-- Table structure for table `saisonReservation`
--

CREATE TABLE IF NOT EXISTS `saisonReservation` (
`idSaisonReservation` int(10) NOT NULL,
  `idSaison` int(1) NOT NULL,
  `idReservation` int(100) NOT NULL,
  `nbreJour` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`idUser` int(100) NOT NULL,
  `loginUser` varchar(50) NOT NULL,
  `pswdUser` varchar(50) NOT NULL,
  `nomUser` varchar(50) NOT NULL,
  `prenomUser` varchar(50) NOT NULL,
  `adUser` varchar(255) NOT NULL,
  `emailUser` varchar(50) NOT NULL,
  `telUser` varchar(10) NOT NULL,
  `idRole` int(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`idUser`, `loginUser`, `pswdUser`, `nomUser`, `prenomUser`, `adUser`, `emailUser`, `telUser`, `idRole`) VALUES
(1, 'kiki', 'kiki', 'kiki', 'kiki', 'maison', 'truc@d.com', '0235555', 1),
(2, 'titi', 'titi', 'titi', 'titi', 'place', 'titi@a.com', '0555', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Activite`
--
ALTER TABLE `Activite`
 ADD PRIMARY KEY (`idActivite`);

--
-- Indexes for table `MomentReservation`
--
ALTER TABLE `MomentReservation`
 ADD PRIMARY KEY (`idMomentReservation`), ADD KEY `idReservation` (`idReservation`), ADD KEY `idMomentSemaine` (`idMomentSemaine`);

--
-- Indexes for table `momentSemaine`
--
ALTER TABLE `momentSemaine`
 ADD PRIMARY KEY (`idMomentSemaine`);

--
-- Indexes for table `reservation`
--
ALTER TABLE `reservation`
 ADD PRIMARY KEY (`idReservation`), ADD KEY `idUser` (`idUser`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
 ADD PRIMARY KEY (`idRole`);

--
-- Indexes for table `saison`
--
ALTER TABLE `saison`
 ADD PRIMARY KEY (`idSaison`);

--
-- Indexes for table `saisonReservation`
--
ALTER TABLE `saisonReservation`
 ADD PRIMARY KEY (`idSaisonReservation`), ADD KEY `idSaison` (`idSaison`,`idReservation`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`idUser`), ADD KEY `roleUser` (`idRole`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Activite`
--
ALTER TABLE `Activite`
MODIFY `idActivite` int(3) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `MomentReservation`
--
ALTER TABLE `MomentReservation`
MODIFY `idMomentReservation` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `momentSemaine`
--
ALTER TABLE `momentSemaine`
MODIFY `idMomentSemaine` int(1) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `reservation`
--
ALTER TABLE `reservation`
MODIFY `idReservation` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
MODIFY `idRole` int(1) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `saison`
--
ALTER TABLE `saison`
MODIFY `idSaison` int(1) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `saisonReservation`
--
ALTER TABLE `saisonReservation`
MODIFY `idSaisonReservation` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `idUser` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
