<?php 

include '../model/user.php';

/**
* Controller for the User model
*/
class UserController
{

	public function getById($id){
		$user = new User();

		return $user;
	}

	public function getByEmail($email){
		$user = new User();

		return $user;
	}

	public function getByEmailAndPassword($email, $password){
		return null;
	}

	public function create($user){
		return false;
	}
	
	public function update($user){
		
	}

	public function deactivate($user){
		$user->isActivated = false;

		return this->update($user);
	}
}