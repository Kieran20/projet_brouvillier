<?php
error_reporting(E_ALL);
class DATABASE {

    private $BD;

    function __construct(){
      $this->connect();
    }

      public function connect()
      {
        try
        {
          $this->BD=new PDO('mysql:host=localhost;dbname=location;charset=utf8','root','root');
        }
        catch(PDOException $e)
        {
          die("Erreur: ". $e->getMessage()."\n");
        }
      }

      // Fonction qui récupère tout les utilisateurs de la BDD
      public function getUsers(){
          $statement =$this->BD->query("select * from user");
          $liste = $statement->fetchAll();
          return $liste;
      }

      /*
          $idUser = identifiant d'un utilisateur
          fonction qui retourne tout les détails d'un utilisateur selon son id
      */
      public function getUserWithID($idUser){
          $req="select * from user where idUser=?;";
          $statement = $this->BD->prepare($req);
          $statement->bindParam(1,$idUser);
          $statement->execute();
          $liste = $statement->fetch();
          return $liste;
      }


      // Fonction pour ajouter un Utilisateur, l'id User s'incrémentera tout seul cette fonction ne retourne rien

      public function addUser($idRole,$login,$pswd,$nom,$prenom,$adresse,$tel,$email){
          $req="insert into user(idRole,loginUser,pswUser,nomUser,prenomUser,adUser,emailUser,telUser) values (?,?,?,?,?,?,?,?)";
          $statement =$this->BD->prepare($req);
          $statement->bindParam(1,$idRole);
          $statement->bindParam(2,$loginUser);
          $statement->bindParam(3,$pswUser);
          $statement->bindParam(4,$nomUser);
          $statement->bindParam(5,$prenomUser);
          $statement->bindParam(6,$adUser);
          $statement->bindParam(7,$emailUser);
          $statement->bindParam(8,$telUser);
          $statement->execute();
        }

      // Fonction qui ajoute ajoute un utilisateur et retourne son ID
      public function addUserReturnID($idRole,$login,$pswd,$nom,$prenom,$adresse,$tel,$email){
          $req="insert into user(idRole,loginUser,pswUser,nomUser,prenomUser,adUser,emailUser,telUser) values (?,?,?,?,?,?,?,?)";
          $statement =$this->BD->prepare($req);
          $statement->bindParam(1,$idRole);
          $statement->bindParam(2,$loginUser);
          $statement->bindParam(3,$pswUser);
          $statement->bindParam(4,$nomUser);
          $statement->bindParam(5,$prenomUser);
          $statement->bindParam(6,$adUser);
          $statement->bindParam(7,$emailUser);
          $statement->bindParam(8,$telUser);
          $statement->execute();
          $idUser = $this->BD->lastInsertId();
        }

      //Fonction qui retourne tous les utilisateurs en fct de leur role
      public function getUserWithIdRole($idRole){
          $req="select * from user where idRole=?;";
          $statement = $this->BD->prepare($req);
          $statement->bindParam(1,$idRole);
          $statement->execute();
          $liste = $statement->fetchAll();
          return $liste;
      }

      //Fonction qui retourne tous les administrateurs du site
      public function getAdministrateurs(){
        $liste = $this->getUserWithIdRole(1);
        return $liste;
      }

      //Fonction qui retourne tous les utilisateurs du site
      public function getUtilisateurs(){
        $liste = $this->getUserWithIdRole(2);
        return $liste;
      }

      //Fonction qui retourne tous les visiteurs du site
      public function getVisiteurs(){
        $liste = $this->getUserWithIdRole(3);
        return $liste;
      }


      //Fonction qui retourne ttes les reservations d'un utilisateur avec son id
      public function getReservationWithUser($idUser){
          $req="select * from reservation where idUser=?;";
          $statement = $this->BD->prepare($req);
          $statement->bindParam(1,$idUser);
          $statement->execute();
          $liste = $statement->fetchAll();
          return $liste;
      }

    //Fonction qui retourne toutes les reservations
    public function getReservation(){
        $statement =$this->BD->query("select * from reservation");
        $liste = $statement->fetchAll();
        return $liste;
    }

    public function getActivite(){
      $statement =$this->BD->query("select * from Activite");
      $liste = $statement->fetchAll();
      return $liste;
    }

      // Fonction qui ajoute une reservation par un utilisateur
      public function addReservation($idUser,$debut,$fin,$nbrePersonne,$prix){
          $req="insert into reservation(idUser,debutReservation,finReservation,nbrePersonne,prixReservation) values (?,?,?,?,?)";
          $statement =$this->BD->prepare($req);
          $statement->bindParam(1,$idUser);
          $statement->bindParam(2,$debut);
          $statement->bindParam(3,$fin);
          $statement->bindParam(4,$nbrePersonne);
          $statement->bindParam(5,$prix);
          $statement->execute();
        }

      public function getLoginPswd($login, $pswd){
        if($login=="kiki"){
          return "ok";
        }
        return "pas ok";
      }


}
