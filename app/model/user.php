<?php 

/**
* Definition of User class.
*/
class User
{
	private $_id;
	private $_firstName;
	private $_lastName;
	private $_email;
	private $_password;
	private $_isActivated;


	public function __construct($_id=0, $_firstName="", $_lastName="", $_email="", $_password="")
	{
		$this->_id = $_id;
		$this->_firstName = $_firstName;
		$this->_lastName = $_lastName;
		$this->_email = $_email;
		$this->_password = $_password;
	}


    public function getId()
    {
        return $this->_id;
    }

    public function setId($_id)
    {
        $this->_id = $_id;

        return $this;
    }

    public function getFirstName()
    {
        return $this->_firstName;
    }

    public function setFirstName($_firstName)
    {
        $this->_firstName = $_firstName;

        return $this;
    }

    public function getLastName()
    {
        return $this->_lastName;
    }

    public function setLastName($_lastName)
    {
        $this->_lastName = $_lastName;

        return $this;
    }

    public function getEmail()
    {
        return $this->_email;
    }

    public function setEmail($_email)
    {
        $this->_email = $_email;

        return $this;
    }

    public function getPassword()
    {
        return $this->_password;
    }

    public function setPassword($_password)
    {
        $this->_password = $_password;

        return $this;
    }

    public function getIsActivated()
    {
        return $this->_isActivated;
    }

    public function setIsActivated($_isActivated)
    {
        $this->_isActivated = $_isActivated;

        return $this;
    }
}