<?php

include_once("header.php");

?>
<h2 style="color:white" class="text-align"> Formulaire de création d'un utilisateur </h2>

<div class="col-lg-12">
  </br>
    <section>
      <form id="formUser" method="post">
        <div class="row">
          <div class="col-lg-6">
            <div class="input-group">
              <span class="input-group-addon">Login : </span>
              <input id="loginUserId" type="text" name="loginUser" placeholder="login" class="form-control"/>
            </div>
            </br>
            <div class="input-group">
              <span class="input-group-addon">Password : </span>
              <input id="pswdUserId" type="text" name="pswdUser" placeholder="password" class="form-control"/>
            </div>
                      </br>
            <div class="input-group">
              <span class="input-group-addon">Nom : </span>
              <input id="nomUserId" type="text" name="nomUser" placeholder="nom" class="form-control"/>
            </div>
        </div>
        <div class="col-lg-6">
          <div class="input-group">
            <span class="input-group-addon">Prenom : </span>
            <input id="prenomUserId" type="text" name="prenomdUser" placeholder="prenom" class="form-control"/>
          </div>
                    </br>
          <div class="input-group">
            <span class="input-group-addon">addresse : </span>
            <input id="adresseUserId" type="text" name="adresseUser" placeholder="adresse" class="form-control"/>
          </div>
          </br>
          <div class="btn-group">
            <button id="btnValidFormUser" name="btnValidFormUser" value="envoyer" type="submit" class="btn btn-success">Valider</button>
          </div>
        </div>
      </form>
    </section>
  </div>
</div>
<script src="../../public/JS/user.js"></script>
