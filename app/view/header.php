<!DOCTYPE html>
<html lang="en">

<html>

    <head>

      <!-- Modele -->
      <?php  include("../location_modele.php") ?>

        <!-- CSS -->

        <meta charset="utf-8" />

        <!-- BOOTSTRAP // JQUERY-->

        <script type="text/javascript" src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
        <link rel="icon" type="image/png" href="../../public/style/img/Logo.png" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
         <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
        <link rel="stylesheet" href="../../public/style/css/base.css" type="text/css" />
        <link rel="stylesheet" href="../../public/style/css/style.css" type="text/css" />

        <title>Gîte de Brouvilliers</title>

    </head>

 <body>
  <header>

        <a class="signInButton" href="#">Connexion</a>
       <h1>Gîte de Brouvilliers</h1>


   <nav class="navbar navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
                  </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav nav-justified">
            <li><a class="limenu" href="home.php">Accueil</a></li>
            <li><a class="limenu" href="#">Visite guidée</a></li>
            <li><a class="limenu" href="view_activite.php">Sorties</a></li>
            <li><a class="limenu" href="view_tarif.php">Tarifs</a></li>
            <li><a class="limenu" href="#">Calendrier</a></li>
          </ul>
        </div>
      </div>
    </nav>
  </header>

  </body>

</html>
