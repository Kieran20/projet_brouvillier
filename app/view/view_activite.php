<?php
  include_once("header.php");
  include("../controller/activite_controller.php");

?>

<h2 style="color:white" class="text-align"> Activités à faire aux alentours du gîte </h2>

<div class="col-lg-12">
  </br>
  <div class="col-lg-6">
    <section>
      <?php  //var_dump($listeActivites); ?>
      <ul class="list-group">
        <?php foreach($listeActivites as $activite){
        echo "<a  class='liste_activite'  id='activite_".$activite['idActivite']."' data-id='".$activite['idActivite']."'><li class='list-group-item'>Activité : ".$activite['nomActivite']."</li></a><li id='description_activite_".$activite['idActivite']."' style='display:none' class='list-group-item'>Description : ".$activite['descriptionActivite']. "<img src='../../public/style/img/activites/".$activite['image']."' alt='alt' height='350' width='350'> </li><li></li>";
         } ?>

      </ul>
    </section>
  </div>

</div>

<script src="../../public/JS/activite.js"></script>
