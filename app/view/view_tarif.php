<?php include_once("header.php") ?>
</br>
<div class="col-lg-6">
  <section id="bodyTarif">
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
          <th scope="col">Saison</th>
          <th scope="col">Par nuit en semaine</th>
          <th scope="col">Par nuit en week-end</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">Été</th>
          <td>35</td>
          <td>50</td>
        </tr>
        <tr>
          <th scope="row">Automne</th>
          <td>25</td>
          <td>50</td>
        </tr>
        <tr>
          <th scope="row">Hiver</th>
          <td>15</td>
          <td>50</td>
        </tr>
        <tr>
          <th scope="row">Printemps</th>
          <td>25</td>
          <td>40</td>
        </tr>
      </tbody>
    </table>

  </section>
</div>
