<?php

include_once("header.php");

$pageTitle = "Accueil - Gîte de Brouvilliers";

?>

<SECTION class="posSlide">
	<div id="slideshow">

		<div class="container">
			<div class="c_slider"></div>
			<div class="slider">
				<figure>
					<img src="../../public/style/img/gite.jpg" alt="" width="550" height="310" />
					<figcaption>The mirror of soul</figcaption>

				</figure>
			<figure>
					<img src="../../public/style/img/gite.jpg" alt="" width="550" height="310" />
					<figcaption>Let's cross that bridge when we come to it</figcaption>
				</figure>
				<figure>
					<img src="../../public/style/img/gite.jpg" alt="" width="550" height="310" />
					<figcaption>Sushi<em>(do)</em> time</figcaption>

				</figure>
				<figure>
					<img src="../../public/style/img/gite.jpg" alt="" width="550" height="310" />
					<figcaption>Waking Life</figcaption>
				</figure>
			</div>
		</div>
	</div>
</SECTION>
